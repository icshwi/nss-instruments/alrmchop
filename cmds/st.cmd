# This should be a test startup script
#!/usr/bin/env iocsh.bash
require alrmchop

# -----------------------------------------------------
# Configure Environment
# -----------------------------------------------------
# Default paths to locate database and protocol
epicsEnvSet("P", "LabS-Embla:")
epicsEnvSet("R", "Chop-Drv-0601:")

iocshLoad("$(alrmchop_DIR)/alrmskf.iocsh")
